/**
 * Much of this code was borrowed from the Drupal 5 autocomplete module and modified to 
 * suit this application. Standing on the shoulders of giants...
 */
 
var dts = {};

/**
 * Attaches the search behaviour to all dynosearcho fields
 */
dts.searchAutoAttach = function () {
  var dtsdb = [];
  $('input.dynosearcho_searchbox').each(function () {
    // hide the search button
    $('div.block-dynosearcho input.form-submit').hide()
    
    // add the autocomplete icon to the input box
    $(this).addClass('form-autocomplete');
    
    // get the delta
    var delta = $(this).parent().siblings('#edit-dynosearcho-delta').val();
    
    // hide results div to start
    var temp = "#dynosearcho-results-" + delta;
    $(temp).hide();
    
    // get the correct url for AHAH
    temp = '#edit-dynosearcho-url-' + delta;
    var uri = $(temp).val() + "/" + delta;
    
    if (!dtsdb[uri]) {
      dtsdb[uri] = new dts.dtsDB(uri,delta);
    }
    new dts.jsAC(this, dtsdb[uri]);
  });
}

/**
 * An AutoComplete object
 */
dts.jsAC = function (input, db) {
  var ac = this;
  this.input = input;
  this.db = db;

  $(this.input).keypress(function (event) { ac.onkeypress(this, event) });
  $(this.input).keyup(function (event) { ac.onkeyup(this, event) });

};

/**
 * Handler for the "keypress" event
 * Used to prevent the enter key used to submit the form here
 */
dts.jsAC.prototype.onkeypress = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 13: // enter
      input.blur();
      e.preventDefault();
      
      //----- If the search module is installed, and the config option is checked, then pass the search terms 
      //-----   along to the search module if the "enter" key is pressed.
      var delta = $(input).parent().siblings('#edit-dynosearcho-delta').val();
      var searchon = $('#edit-dynosearcho-integrated-search-' + delta).val();
      if (searchon == 1) {
	      //----- figure out the correct path to the Drupal search
	      var basepath = $('#edit-dynosearcho-basepath-' + delta).val();
	      var useq = $('form#dynosearcho_search_form_' + delta).attr('action');
	      if (useq.indexOf("?q=") > 0) {
	        window.location.href = basepath + "?q=search/node/" + Drupal.encodeURIComponent(this.input.value);
	      } else {
	        window.location.href = basepath + "search/node/" + Drupal.encodeURIComponent(this.input.value);
	      }
	    }
  }
}

/**
 * Handler for the "keyup" event
 */
dts.jsAC.prototype.onkeyup = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 16: // shift
    case 17: // ctrl
    case 18: // alt
    case 20: // caps lock
    case 33: // page up
    case 34: // page down
    case 35: // end
    case 36: // home
    case 37: // left arrow
    case 38: // up arrow
    case 39: // right arrow
    case 40: // down arrow
    case 9:  // tab
    case 27: // esc
      return true;
      
    default: // all other keys
      if (input.value.length > 0) {
        this.doSearch();
      } else {
        this.popup.hide();
        this.popup.empty();
      }
      return true;
  }
}

/**
 * Starts a search
 */
dts.jsAC.prototype.doSearch = function () {
  // Show popup
  var resultsDiv = "div#dynosearcho_results_" + this.db.delta;
  this.popup = $(resultsDiv);
  this.popup.hide();
  this.popup.empty();
  
  this.db.owner = this;
  this.db.search(this.input.value);
}


/**
 * Fills the div with any matches received
 */
dts.jsAC.prototype.found = function (matches) {
  // If no value in the textfield, do not show the results.
  if (!this.input.value.length) {
    return false;
  }

  // Prepare matches
  var ul = document.createElement('ul');
  var ac = this;
  var len = matches.length;
  for (var key = 0; key < len; key++) {
    var li = document.createElement('li');
    $(li).html(matches[key]);
    li.autocompleteValue = key;
    $(ul).append(li);
  }

  // Show popup with matches, if any
  if (this.popup) {
    if (ul.childNodes.length > 0) {
      $(this.popup).empty().append(ul).fadeIn('slow');
    }
  }
}

dts.jsAC.prototype.setStatus = function (status) {
  switch (status) {
    case 'begin':
      $(this.input).addClass('throbbing');
      break;
    case 'cancel':
    case 'error':
    case 'found':
      $(this.input).removeClass('throbbing');
      break;
  }
}

/**
 * An dynosearcho DataBase object
 */
dts.dtsDB = function (uri, delta) {
  this.uri = uri;
  this.delta = delta;
  this.delay = 300;
  this.cache = {};
}

/**
 * Performs a cached and delayed search
 */
dts.dtsDB.prototype.search = function (searchString) {
  var db = this;
  this.searchString = searchString;

  // See if this key has been searched for before
  if (this.cache[searchString]) {
    return this.owner.found(this.cache[searchString]);
  }

  // Initiate delayed search
  if (this.timer) {
    clearTimeout(this.timer);
  }
  this.timer = setTimeout(function() {
    db.owner.setStatus('begin');

    // Ajax GET request for autocompletion
    $.ajax({
      type: "GET",
      url: db.uri +'/'+ Drupal.encodeURIComponent(searchString),
      success: function (data) {
        // Parse back result
        var matches = Drupal.parseJson(data);
        if (typeof matches['status'] == 'undefined' || matches['status'] != 0) {
          db.cache[searchString] = matches;
          // Verify if these are still the matches the user wants to see
          if (db.searchString == searchString) {
            db.owner.found(matches);
          }
          db.owner.setStatus('found');
        }
      },
      error: function (xmlhttp) {
        alert('An HTTP error '+ xmlhttp.status +' occured.\n'+ db.uri);
      }
    });
  }, this.delay);
}

/**
 * Cancels the current autocomplete request
 */
dts.dtsDB.prototype.cancel = function() {
  if (this.owner) this.owner.setStatus('cancel');
  if (this.timer) clearTimeout(this.timer);
  this.searchString = '';
}

dts.searchTypeAutoAttach = function () {
  //----- set the default state
  $('fieldset#dynosearcho_nodesearch').hide();
  $('fieldset#dynosearcho_menusearch').hide();
  switch($('input.dynosearcho_searchtype:checked').val()) {
    case 'node':
      $('fieldset#dynosearcho_nodesearch').show();
      break;
    case 'menu':
      $('fieldset#dynosearcho_menusearch').show();
      break;
  }
 
  //----- update the boxes as radio buttons are clicked
  $('input.dynosearcho_searchtype').click(function () {
    if ($(this).val() == 'node') {
      $('fieldset#dynosearcho_menusearch').hide();
      $('fieldset#dynosearcho_nodesearch').slideDown();
    } 
    else {
      $('fieldset#dynosearcho_nodesearch').hide();
      $('fieldset#dynosearcho_menusearch').slideDown();
    } 
  });
}

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(dts.searchAutoAttach);
  $(document).ready(dts.searchTypeAutoAttach);
}

