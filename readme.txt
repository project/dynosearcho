DESCRIPTION
--------------------------
DynoSearcho lets users dynamically search for different types of content using AJAX.

This module allows the site admin to display various blocks on the site that provide 
a dynamic way to search for content. DynoSearch uses AHAH to display results as the 
user enters them (similar to Drupal's most-excellent autocomplete feature). Each block
can be configured to display a configurable number of results from specific node types.

INSTALLATION
--------------- 
- Enable dynosearcho module.
- Define how many DynoSearcho blocks you would like available (each one can have 
  different search settings) on admin/settings/dynosearcho.
- Place and configure each DynoSearcho block from the block administration screen
  on admin/build/block. For the most part, I think the block settings form is
  self-explanatory. Let me know if you think otherwise.
- That's it!

TODO/BUGS/FEATURE REQUESTS
----------------
- add ability to search users
- see http://drupal.org/project/issues/dynosearcho

NOTES
----------------
Warning: The LightBox2 module seems to interfere with DynoSearcho when both modules are 
loaded on the same page. It appears that there is some sort of JavaScript conflict going
on, but I have been unable to track it down so far. The bug _may_ have something to do
with the scoping of the eval function in the parseJson function of DynoSearcho 
(http://service.zimki.com/user/blog/tomi/2006/08/07/javascript-eval). The bug has 
been confirmed with Drupal 4.7.

CREDITS
----------------------------
Authored and maintained by michael anello <manello AT gmail DOT com> (Drupal username: ultimike)
Sponsored by Ozmosis - http://www.ozmosis.com/
